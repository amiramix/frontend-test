import React, { useState } from 'react';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import { makeStyles } from '@material-ui/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const getModalStyle = () => ({
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)'
});

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'right'
  },
  textField: {
    marginLeft: 20,
    marginRight: 20,
    width: 320,
  },
  button: {
    margin: 20,
    marginTop: 50,
    width: 100
  },
  dl: {
    margin: 20,
    width: 360
  },
  dt: {
    marginTop: 20
  },
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: "#ffffff",
    border: '2px solid #000',
    boxShadow: '#333333',
    padding: 20
  },
}));

const DELETE_CONTACT = gql`
mutation DeleteContact($id: ID!) {
  deleteContact(
      id: $id
  )
}
`

const ConfirmDelete = ({ item, onClose }: any) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [deleteContact, { loading, error, data }] = useMutation(DELETE_CONTACT);

  const onDelete = (id: string): void => {
    deleteContact({ variables: { id } });
  }

  if (data && !error) {
    onClose(item.id);
  }

  return (
    <Modal
      aria-labelledby="simple-modal-title"
      open={true}
    >
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Please confirm to delete</h2>

        {loading && <div>Saving ...</div>}
        {error && <div>Something went wrong ...</div>}

        {!loading && !error && (
          <Box className={classes.container}>
            <Box component="dl" className={classes.dl}>
              <Box component="dt" fontWeight="fontWeightBold" className={classes.dt}>Name:</Box><Box component="dd">{item.name}</Box>
              <Box component="dt" fontWeight="fontWeightBold" className={classes.dt}>Email:</Box><Box component="dd">{item.email}</Box>
            </Box>
            <Box>
            <Button
              variant="contained"
              className={classes.button}
              onClick={() => onClose(undefined)}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={() => onDelete(item.id)}
            >
              Delete
            </Button>
            </Box>
          </Box>
        )}
      </div>
    </Modal>
  );
}

export default ConfirmDelete;
