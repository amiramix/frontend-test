import React, { useState } from 'react';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import { makeStyles } from '@material-ui/styles';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const getModalStyle = () => ({
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)'
});

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'right'
  },
  textField: {
    marginLeft: 20,
    marginRight: 20,
    width: 360,
  },
  button: {
    margin: 20,
    marginTop: 50,
    width: 100
  },
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: "#ffffff",
    border: '2px solid #000',
    boxShadow: '#333333',
    padding: 20
  },
}));

const ADD_CONTACT = gql`
mutation AddContact($name: String!, $email: String!) {
  addContact(
    contact: {
      name: $name,
      email: $email
    }
  ) {
    id, name, email
  }
}
`

const UPDATE_CONTACT = gql`
mutation UpdateContact($id: ID!, $name: String!, $email: String!) {
  updateContact(
    contact: {
      id: $id,
      name: $name,
      email: $email
    }
  ) {
    id, name, email
  }
}
`

const EditContact = ({ item, onClose }: any) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [name, setName] = useState(item.name);
  const [email, setEmail] = useState(item.email);
  const [saveContact, { loading, error, data }] = useMutation(item.id ? UPDATE_CONTACT : ADD_CONTACT);

  const onSave = (name: string, email: string): void => {
    saveContact({ variables: { id: item.id, name, email } });
  }

  if (data && !error) {
    onClose(item.id ? data.updateContact : data.addContact);
  }

  return (
    <Modal
      aria-labelledby="simple-modal-title"
      open={true}
    >
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Edit Contact</h2>

        {loading && <div>Saving ...</div>}
        {error && <div>Something went wrong ...</div>}

        {!loading && !error && (
          <form className={classes.container} noValidate autoComplete="off">
            <TextField
              label="Name"
              className={classes.textField}
              value={name || ''}
              onChange={e => setName(e.target.value)}
              margin="normal"
            />
            <TextField
              label="Email"
              className={classes.textField}
              value={email || ''}
              onChange={e => setEmail(e.target.value)}
              margin="normal"
            />
            <Button
              variant="contained"
              className={classes.button}
              onClick={() => onClose(undefined)}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={() => onSave(name, email)}
            >
              Save
          </Button>
          </form>
        )}
      </div>
    </Modal>
  );
}

export default EditContact;
