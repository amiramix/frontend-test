import React, { useState } from 'react';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import ListView from './ListView';
import EditContact from './EditContact';
import ConfirmDelete from './ConfirmDelete';
import { listItem } from './types';
import "./App.css";

const useStyles = makeStyles(() => ({
  button: {
    margin: 20,
    marginTop: 50,
    width: 100
  }
}));

const GET_CONTACTS = gql`
query {
  contacts {
    name, email, id
  }
}
`

const contactsForDisplay = (contacts: Array<listItem>): Array<listItem> =>
  contacts ? Object.values(contacts).sort(
    (a: listItem, b: listItem): any => (`${a.name} ${a.email}` > `${b.name} ${b.email}`)
  ) : [];

const contacsFromAPI = (contacts: Array<object>): any => contacts.reduce(
  (prev: object, curr: any): any => Object.assign(prev, { [curr.id]: curr }),
  {});

const App = () => {
  const classes = useStyles();
  const [contacts, setContacts]: [any, Function] = useState(undefined);
  const [itemToEdit, setItemToEdit]: [any, Function] = useState(undefined);
  const [itemToDelete, setItemToDelete]: [any, Function] = useState(undefined);

  const onEdit = (item: listItem) => () => setItemToEdit(contacts[item.id]);
  const onDelete = (item: listItem) => setItemToDelete(contacts[item.id]);

  return (
    <div className="App">
      <header className="App-header">
        <h1>Contacts</h1>
      </header>
      <div>
        <Query query={GET_CONTACTS}>
          {({ loading, error, data }: any): any => {
            if (loading) {
              return <div>Loading ...</div>;
            }
            if (error) {
              return <div>Something went wrong ...</div>;
            }

            if (!contacts) {
              setContacts(contacsFromAPI(data.contacts));
            }

            return (
              <ListView
                items={contactsForDisplay(contacts)}
                onEdit={onEdit}
                onDelete={onDelete}
              />
            )
          }}
        </Query>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => setItemToEdit({})}
        >
          Add
        </Button>
        {itemToEdit && <EditContact
          item={itemToEdit}
          onClose={(item: listItem) => {
            if (item) {
              setContacts(Object.assign(contacts, { [item.id]: item }));
            }
            setItemToEdit(undefined);
          }}
        />}
        {itemToDelete && <ConfirmDelete
          item={itemToDelete}
          onClose={(id: string) => {
            if (id) {
              delete contacts[id];
              setContacts(contacts);
            }
            setItemToDelete(undefined);
          }}
        />}
      </div>
    </div>
  );
}

export default App;
