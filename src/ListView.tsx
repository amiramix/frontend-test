import React from "react";
import { makeStyles } from '@material-ui/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { listItem } from './types';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center'
  },
  list: {
    width: '100%',
    maxWidth: 640,
  },
}));

const getListItem = (item: listItem, onEdit: Function, onDelete: Function): any => (
  <ListItem button key={item.id} onClick={onEdit(item)}>
    <ListItemText primary={item.name} />
    <ListItemText primary={item.email} />
    <ListItemSecondaryAction>
      <IconButton edge="end" aria-label="delete" onClick={() => onDelete(item)} >
        <DeleteIcon />
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
);

const ListView = ({ items, onEdit, onDelete }: { items: Array<listItem>, onEdit: Function, onDelete: Function }) => {
  const classes: any = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.list}>
        <List aria-label="main contacts list">
          {items.map((item: listItem) => getListItem(item, onEdit, onDelete))}
        </List>
      </div>
    </div>
  )
}

export default ListView;
